# monitor.ligo.org

## the CUBE visualization of low latency related services

- replaced by the [cube plugin](https://dashboard.igwn.org/icingaweb2/cube/services?dimensions=service.vars.cube_dimension%2Cservice.vars.cube_name&showSettings=0&(service.vars.environment=Production|service.vars.environment=Backup)) of dashboard.igwn.org
