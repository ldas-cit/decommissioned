# bugs.ligo.org


## The original Redmine bugs.ligo.org

- bug tracker was decommissioned in 2019.
- Bug reports should now be submitted to the [LIGO git dashboard](https://git.ligo.org/dashboard/issues).
