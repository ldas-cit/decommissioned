# jupyter3.ligo.caltech.edu

## jupyter notebook server
- decommissioned December 2023
- replaced by jupyter4.ligo.caltech.edu
- was based on docker swarm, aimed at being replaced by a kubernetes run service. jupyter4 is a single VM instance. 
