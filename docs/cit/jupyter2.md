# jupyter2.ligo.caltech.edu

## jupyter notebook server
- decommissioned August 2024 because of unfixed bugs and low usage
- users are invited to migrate over https://jupyter4.ligo.caltech.edu 
- contact: LDAS_ADMIN_CIT <ldas_admin_cit@ligo.caltech.edu>
