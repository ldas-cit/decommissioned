## Title of the page: suggest the fqdn of the service

+ bullet point
+ create a page in the docs directory of the repository
+ add it to the navigation bar by editing the mkdocs.yaml file at the root of the repository

## suggested content and structure of the page

- 1-2 paragraphs summary of the service
- why and when it was decommissioned
- if applicable,  a link to a replacment / equivalent / new instance
- if applicable, links to git projects, archives, documentations
- if applicable, contact to person / group likely to provide historical information or access.
